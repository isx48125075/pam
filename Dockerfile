#versio 0.0.1
#hostpam
#-------------------------

FROM fedora:27
LABEL author="@edt ASIX M06-ASO"
LABEL description="host pam 2018-19"
RUN dnf -y install openldap-clients passwd procps nss-pam-ldapd 
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/install.sh /opt/docker/startup.sh
WORKDIR /opt/docker
CMD ["/opt/docker/startup.sh"]
